aws-lambda-template
===================

Overview
--------

This repository contains a template for creating AWS Lambda functions. It uses:

- [TypeScript](http://www.typescriptlang.org/) for typeful programming
- [tslint](https://palantir.github.io/tslint/) for code linting
- [prettier](https://prettier.io/) for code formatting
- [winston](https://github.com/winstonjs/winston/tree/2.4.0) for structured logging
- [node-lambda](https://github.com/motdotla/node-lambda) for local testing of the Lambda and for deployment

Getting Started
---------------

- Fork this project
- Update `package.json` for 'name', 'version', 'description', 'repository', 'author'
- Update `deploy-<env_name>.env` to specify all of the non-secret environment variables needed by the application at runtime in each environment 
- Update `events/sample-event.json` to include a sample of the type of input event that this lambda will expect to receive. 
  - This is used for reference only, but it is useful to always have one to refer to during development
- Add any necessary `yarn dependencies`
- Add your functionality to `src/index.ts`
- Update `.circleci/config.yml` to specify 
  - the build docker container
  - lambda environment configuration
  - the per environment `AWS_FUNCTION_NAME`
  - the per environment `AWS_ROLE_ARN`
- Update this README file

Usage
-----

### Running Locally

You can run the Lambda locally with the default test event with the `run-lambda` script:

```
yarn run-lambda
```

The `--help` flag will display all the options for this script, but two of the key ones are `-j` and `-x`, which allow you to specify a custom event file and context file respectively.

### Building

The build process (`yarn run build`) executes against `src/` and outputs to `build/`. It performs the following actions:

- remove `build/` if it exists
- Compile code in the `src/` directory using `tsc` into `build/`
- execute `tools/build.js` to copy the `package.json` into the build directory
- execute `yarn install --production` in the context of the `build` directory

### Deploying

#### Locally to Sandbox
To deploy locally customize and execute the following command:

```
AWS_DESCRIPTION="<Your lambda description>" \
AWS_MEMORY_SIZE=128 \
AWS_TIMEOUT=5 \
AWS_RUNTIME=nodejs8.10 \
AWS_REGION=us-west-2 \
AWS_HANDLER=index.handler \
AWS_PROFILE=<profile from ~/.aws/credentials> \
AWS_FUNCTION_NAME=<your lambda name> \
AWS_ROLE_ARN="<your role arn>" \
PREBUILT_DIRECTORY=build \
node-lambda deploy --configFile deploy-sandbox.env
```

#### CircleCI

- Ensure AWS credentials are configured in the `circleci.com` settings for the branch
