import fs from 'fs-extra';
import pkg from '../package.json';

/**
 * Compiles the project from source files into a distributable
 * format and copies it to the output (build) folder.
 */
async function build() {

  // Write minimal package.json to build directory
  await fs.writeFile('build/package.json', JSON.stringify({
    ...pkg,
    main: 'index.js',
  }));
}

build();
