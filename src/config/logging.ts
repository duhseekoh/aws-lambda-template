import winston = require('winston');
import { LoggerInstance } from 'winston';

type LoggerOptions = {
  label?: string;
  meta?: object;
};

export default {
  getLogger: (options: LoggerOptions): LoggerInstance =>
    new winston.Logger({
      rewriters: [(level, msg, meta) => ({ ...options.meta, ...meta })],
      transports: [
        new winston.transports.Console({
          logstash: true,
          timestamp: true,
          label: options.label,
        }),
      ],
    }),
};
