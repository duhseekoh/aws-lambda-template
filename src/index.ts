import Logging from './config/logging';
import path from 'path';

type LambdaContext = {
  awsRequestId: string;
};

export const handler = (
  event: any,
  context: LambdaContext,
  callback: (err: any, res: any) => void,
) => {
  const log = Logging.getLogger({
    label: path.basename(__filename),
    meta: {
      awsRequestId: context.awsRequestId,
    },
  });

  log.info('eventReceived', { event });

  // Example of propagating the results of an async function call to the Lambda
  // callback
  russianRoulette(context.awsRequestId, event)
    .then(res => callback(null, res))
    .catch(err => callback(err, null));
};

async function russianRoulette<A>(s: string, a: A): Promise<A> {
  const isIndexOfBullet = (n: number) => {
    const numChambers = 6;
    return n % numChambers === 0;
  };

  const charCodeAtHead = s.charCodeAt(0);

  if (isNaN(charCodeAtHead) || isIndexOfBullet(charCodeAtHead)) {
    throw new Error('You lose');
  }

  return a;
}
